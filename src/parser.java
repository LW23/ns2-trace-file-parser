import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/*
 * Lewis Wilson
 * Lewiswilson1497@gmail.com
 * 23/11/19
 */

public class parser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Format to two decimal places
		DecimalFormat f = new DecimalFormat("##.00");

		int received = 0, dropped = 0, sentPackets = 0;
		double delay = 0;

		// Hash map of the format <PktID, Time>
		Map<String, String> hm = new HashMap<String, String>();

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader("/home/lewis/F29DC/Part34/10/out.tr"));
			String line = reader.readLine();

			while (line != null) {

				// Split on single spaces
				String[] splited = line.split("\\s+");

				
				if (splited[4].equals("tcp") || splited[4].equals("udp") || splited[4].equals("cbr")) {

					if (splited[0].equals("-")) {
						sentPackets++;
						hm.put(splited[11], splited[1]);
					} else if (splited[0].equals("d")) {
						dropped++;
					} else if (splited[0].equals("r")) {
						received++;
					}

				}

				// Working out time
				if (splited[0].equals("r") && hm.containsKey(splited[11])) {

					double sentTime = Double.parseDouble(hm.get(splited[11]));
					double recivedTime = Double.parseDouble(splited[1]);

					delay += recivedTime - sentTime;
				}
				line = reader.readLine();
			}
			
			// Calculations

			double throughput = ((double) received / (double) sentPackets) * 100;

			int totalLostPackets = sentPackets - received;

			// Outputting results

			System.out.println("Results");
			System.out.println("=======");

			System.out.println("The throughput is " + f.format(throughput) + "%");
			System.out.println("Total dropped packets " + dropped);
			System.out.println("Total lost packets " + totalLostPackets);

			System.out.println("Average delay for a packet " + f.format(delay / received)+ "ms");

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
